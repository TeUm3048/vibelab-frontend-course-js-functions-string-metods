"use strict";

const GENRE_COUNT = 3;

const numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?");

const personalMovieDB = {
  count: numberOfFilms,
  movies: {},
  actors: {},
  genres: [],
  privat: false,
};

/**
 * The function "showMyDB" returns the database object if it does not have a "privat" property,
 * otherwise it returns null.
 * @param database - The database parameter is an object that represents a database.
 * @returns {database | null} the database object if the "privat" property is not present or is falsy. Otherwise, it
 * returns null.
 */
function showMyDB(database) {
  if (!database?.privat) {
    return database;
  }
  return null;
}

/**
 * The function "writeYourGenres" prompts the user to enter their favorite genres and stores them in an
 * array.
 * @returns {[string | null]} an array of genres.
 */
function writeYourGenres() {
  let genres = [];
  for (let i = 1; i <= GENRE_COUNT; i++) {
    let genre = prompt(`Ваш любимый жанр под номером ${i}`);
    genres.push(genre);
  }
  return genres;
}

/**
 * The function `validateMovie` checks if a movie is valid based on its length and if it already exists
 * in a database.
 * @param {string | null} movie - The `movie` parameter is a string that represents the name of a movie.
 * @returns {boolean} The function `validateMovie` returns `true` if the `movie` meets all the validation
 * criteria, and `false` otherwise.
 */
const validateMovie = (movie) => {
  if (movie === null) {
    return false;
  }
  if (movie.length <= 0 || movie.length > 50) {
    return false;
  }
  if (movie in personalMovieDB.movies) {
    return false;
  }
  return true;

  // Или в одну строку
  // return (
  //   movie.length > 0 && movie.length <= 50 && !(movie in personalMovieDB.movies)
  // );
};

/* The `for` loop is used to prompt the user for the name and score of each movie they have watched. */
for (let i = 0; i < numberOfFilms; i++) {
  let movie = prompt("Один из последних просмотренных фильмов?");
  let movieScore = prompt("На сколько оцените его?");

  /* The `while` loop is used to validate the user input for the movie name. */
  while (!validateMovie(movie)) {
    movie = prompt(
      "Имя не должно быть пустым, длиннее 50 символов и повторятся.\n\
       Введите один из последних просмотренных фильмов"
    );
    movieScore = prompt("На сколько оцените его?");
  }

  /* Add a new key-value pair `movie: movieScore` to the 
  `movies` property of the `personalMovieDB` object. */
  personalMovieDB.movies[movie] = movieScore;
}

console.log(personalMovieDB);
